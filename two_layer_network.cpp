/*
 * Programming Exercise 3
 * 108012209500 Benjamin Lentz
 * 108012234853 Emanuel Durmaz
 */

#include "two_layer_network.h"

#include <cassert>


/// Creates a two layer network with M neurons (+1 bias neuron) in the
/// hidden layer.
ann::two_layer_network::two_layer_network (size_t M_a, real_t eta_a)
// @hint: Initialization to correct sizes is offered for free this time.
//        Note that w2_m and zm_m include the bias neuron.
//        The layout of the weights and z_m can be arranged in many ways. This
//        is just a suggestion, choose what you prefer.
//  If you change this, please ensure that function M (implemented in the header)
//  still works.
: eta_m(eta_a),
  w1_m(M_a, 2), // return M_a with M()
  w2_m(1, M_a+1),
  zm_m(M_a+1, 1, 1) //initialize with ones
{


}


/// Initializes the networks weights with random values from the given interval.
void ann::two_layer_network::initialize_weights (real_t lower_a, real_t upper_a)
{
    // @task: Initialize all weights randomly.  4.1f
    w1_m.fill_with_uniform_samples(lower_a,upper_a);
    w2_m.fill_with_uniform_samples(lower_a,upper_a);
}


/// Activates the network: Computes network output y for a given input x.
ann::two_layer_network::real_t ann::two_layer_network::activate (real_t x1_a) const
{
    // @hint: It might be convenient to put the input in a column vector together
    //        with bias x_0 = 1.
    //matrix x_l(2, 1, 1);
    //x_l(1, 0) = x1_a;

    // @task: Compute the networks output y for input x.  4.1e
    //        Please make sure to store the intermediate values z_m, e.g. in
    //        member variable zm_m.
    //        Not reusing those values during training costs points.

    //calculate zm_m
    for(size_t m=1; m < zm_m.rows(); m++) // <= M()
    { //     1...M()                                   m = 0...M()-1
        zm_m(m,0) = activation_function(w1_m(m-1,0) + w1_m(m-1,1) * x1_a);
    }

    //calculate y
    real_t sum = 0.0;
    for(size_t m=1; m < zm_m.rows(); m++) // <= M()
    {
        sum = sum + ( w2_m(0,m)  * zm_m(m,0) );
    }
    real_t y = w2_m(0,0) + sum;

    return y;
}


// Trains the network with the given input and target output; i.e. does one
// step of gradient descent for the given sample. In yet other words, this
// implements error backpropagation.
void ann::two_layer_network::train (real_t x_a, real_t t_a)
{
    // @task: Train the network as described in 4.1a.
    real_t y = activate(x_a);
    real_t delta = y - t_a;

    matrix delta_m(M()+1,1,1);

    //calculate deltas
    for (size_t m = 0; m <= M(); m++)
    {
        delta_m(m,0) = zm_m(m,0) * (1 - zm_m(m,0) ) * delta * w2_m(0,m);
    }

    //one step of gradient descent
    w2_m(0,0) = w2_m(0,0) + ( -eta_m * delta * 1.0 );

    for (size_t m = 1; m <= M(); m++) //skip m = 0
    {
        w2_m(0,m) = w2_m(0,m) + ( -eta_m * delta * zm_m(m,0) );

        // 0...M-1                              1...M
        w1_m(m-1,0) = w1_m(m-1,0) + ( -eta_m * delta_m(m,0) * 1);
        w1_m(m-1,1) = w1_m(m-1,1) + ( -eta_m * delta_m(m,0) * x_a);
    }
}


/// Trains the network with multiple training samples (an epoch).
/// Each row of the given matrix is expected to contain input x and target
/// output t.
void ann::two_layer_network::train_epoch (const matrix& samples_a)
{
    // @task: Train the network with each of the given samples.  4.2b

    //train
    for(size_t p = 0; p < samples_a.rows(); p++)
    {
        real_t x = samples_a(p,0);
        real_t t = samples_a(p,1);
        train(x,t);
    }
}


/// The activation function f_act used in the hidden layer
ann::two_layer_network::real_t ann::two_layer_network::activation_function (real_t value_a) const
{
    // @task: Implement the activation function.  4.1e
    return 1.0 / (1.0 + std::exp(-value_a) );
}

