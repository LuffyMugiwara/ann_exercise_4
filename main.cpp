/* Programming Exercise 3
   Work is do be done in main.cpp and two_layer_network.cpp.
   Please find the usual  @task  markers and follow their instructions.
   There are no markers to remind of the questions to be answered for 4.2c,d.
   There is also at least one  @hint.
   Please feel free to fully adapt the code to your likings. You may even
   implement the network without using a class, if you find that
   easier. Using the template is recommended, though.

   Please leave your names here.
   author(s):

    108012209500 Benjamin Lentz
    108012234853 Emanuel Durmaz

    4.2 (c)
    The learning process of the network needs more time the bigger the complexity M of the networks.
    That is because much more weights have to be calculated each epoch. This leads to a noticeable longer learning time because of the high number of epochs.
    For every complexity the error values differ the most in the first epochs. They get lower with every epoch. Also the differences of the error decreases from epoch to epoch.
    The error values for the different runs vary the most for M=60. Where in M=10 and M=2 they differ less.
    But after the last epoch the error between the different runs varies the most for M=2 while the differences are less for M=10 and M=60.

    4.2 (d)
    Regarding to the values we can observe in our diagrams, the Network approximates the given function the best when it has a high complexity.
    With a low complexity it only can rougly approximate it so that several Minima and maxima are left out.

*/

#include "two_layer_network.h"
#include "matrix.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <string>

// @hint: Use shorthands to commonly used types.
typedef ann::matrix matrix;
typedef ann::two_layer_network network;
typedef network::real_t real_t;
typedef network::size_t size_t;
typedef std::string string;

//declare functions
void write_data (const string& filename_a, const matrix& data_a);

// Returns the square of the given value.
real_t square (real_t value_a)
{
  return value_a * value_a;
}

/// Takes number_of_points_a samples of the network between xmin_a and xmax_a
/// and writes them to a text file where each line contains one sample (x and y).
void sample_network (const string& filename_a, const network& network_a, real_t xmin_a, real_t xmax_a, size_t number_of_points_a)
{
    // @task: Compute the networks output for values in the given range and write
    //        input and output to a text file.  4.1f
    // @hint: In case you forgot how to write to file, take a look at the next
    //        function  write_data.

    real_t range = xmax_a - xmin_a;
    real_t stepsize = range / (number_of_points_a - 1); //include xmax_a
    //std::cout << "range: " << range << std::endl;
    //std::cout << "stepsize: " << stepsize << std::endl;

    matrix data(number_of_points_a, 2);

    real_t x = xmin_a;
    for(size_t i = 0; i < number_of_points_a; i++)
    {
        real_t y = network_a(x); // the same like network_a.activate(x);
        data(i,0) = x;
        data(i,1) = y;

        //std::cout << "i: " << i << " x: " << x << " y: " << y << std::endl;
        x += stepsize;
    }
    //std::cout << data << std::endl;

    write_data(filename_a,data); //save to file
}

/// Writes content of a matrix to file in a plottable format.
void write_data (const string& filename_a, const matrix& data_a)
{
  std::ofstream out_l(filename_a);
  for (size_t row_l = 0; row_l < data_a.rows(); ++row_l)
  {
    for (size_t col_l = 0; col_l < data_a.cols() - 1; ++col_l)
    {
      out_l << data_a(row_l, col_l) << '\t';
    }
    out_l << data_a(row_l, data_a.cols() - 1) << std::endl;
  }
}

/// Returns the MSSE of the network on the samples.
/// Each row of the given matrix is expected to contain input x and target
/// output t.
real_t compute_error (const matrix& samples_a, const network& net_a)
{
  // @task: Compute the MSSE as defined in the formula in  4.2c.
    real_t P = samples_a.rows();
    real_t sum = 0.0;

    for(size_t p=0; p < P; p++)
    {
        real_t x_p = samples_a(p,0);
        real_t t_p = samples_a(p,1);
        real_t y = net_a(x_p); //same like net_a.activate(x_p)

        sum = sum + square(y - t_p);
    }

    return (1.0/P) * sum;
}


int main (int argc, char** argv)
{
    // Some more shorthands that might be useful.
    using std::exp;
    using std::sin;
    using std::string;
    using std::to_string;
    using std::ofstream;
    using std::cout;
    using std::endl;

    // @task: Create plot data for a randomly initialized network as described in 4.1f.
    {
        size_t M = 20;
        real_t eta = 0.01;
        network netw(M,eta);
        netw.initialize_weights(-5,5);

        //save to file
        sample_network("netw_20_untrained.txt",netw, -7.5, 7.5, 1000);
    }

    // @task: Create training samples as described in  4.2b.
    // @task: Don't forget to write them into a text file for plotting, as
    //        demanded in 4.2d

    //create training set
    size_t P = 50;
    matrix samples(P,2);
    matrix rand_m(P,1);
    rand_m.fill_with_uniform_samples(-0.15,0.15);

    for(size_t p = 0; p < P; p++)
    {
        real_t x_p = ( (15.0*p)/(P-1.0) ) - 7.5;
        real_t t_p = std::exp(-square(x_p) / 10.0) * std::sin(x_p) + rand_m(p,0);

        samples(p,0) = x_p;
        samples(p,1) = t_p;
    }
    write_data("training_set.txt",samples);


    // @task: Do various training runs as described in 4.2c.
    //        Use the same set of training samples for every epoch.
    //        Write the epoch's number (iteration counter) and the error of the
    //        network (see compute_error) to file for each epoch.
    // @hint: You might want to consider to decrease the number of epochs for testing.

    // @task: After the maximum number of epochs, sample each network once in the
    //        same way (and with the same parameters) as done in 4.1f.  4.2d

    size_t M[] = {2, 10, 60};
    size_t M_len = sizeof(M) / sizeof(*M);
    size_t num_of_training_runs = 5;

    real_t eta = 0.01;
    size_t num_of_epochs = 10000;

    for(size_t i=0; i < M_len; i++) //iterate over M
    {
      std::cout << "M: " << M[i] << std::endl;

        for(size_t j=1; j <= num_of_training_runs; j++)
        {
            std::cout << j << ". training run" << std::endl;

            network netw(M[i], eta);
            matrix msse(num_of_epochs, 2);
            netw.initialize_weights(-5.0, 5.0); //initialize new weights for every new training run

            for (size_t k = 0; k < num_of_epochs; k++) //training
            {
                netw.train_epoch(samples);
                real_t error = compute_error(samples, netw);

                //save error value
                msse(k, 0) = k; //index of epoch
                msse(k, 1) = error;
            }

            //save to files
            sample_network("netw_" +std::to_string(M[i]) + "_" + std::to_string(j) + ".txt", netw, -7.5, 7.5, 1000);
            write_data("error_" + std::to_string(M[i]) + "_" + std::to_string(j) + ".txt", msse);
        }

    }

    return 0;
}
